// 3. Создать массив, который будет хранить в себе информацию о сотрудниках предприятия. Каждый элемент масива — объект,
//    содержащий свойства: name, sName, age, occupation, и метод show, который выводит всю информацию о пользователе.
//    Реализовать заполнение массива пользователем. По окончанию заполнения — вывести информацию о сотрудниках.
const form = document.querySelector('form');
const nameInput = document.getElementById('name');
const surnameInput = document.getElementById('surname');
const ageInput = document.getElementById('age');
const occupationInput = document.getElementById('occupation');
const submitBtn = document.getElementById('submit-btn');
const showBtn = document.getElementById('show-btn');
const workersDiv = document.querySelector('.workers');
let workers = [];

submitBtn.addEventListener('click', e => e.preventDefault())
submitBtn.addEventListener('click', submitNewWorker)
function submitNewWorker() {
  const inputs = [...document.querySelectorAll('input')];
  for (let inp of inputs) {
    // handle empty fields
    if (!inp.value) {
      alert('All fields must be filled');
      return;
    }
  }
  const newWorker = {
    name: nameInput.value,
    surname: surnameInput.value,
    age: ageInput.value,
    occupation: occupationInput.value,
  }
  workers.push(newWorker);
  // clear inputs after submitting
  inputs.forEach(inp => inp.value = '');
  // clear workers div after submitting
  workersDiv.innerHTML = '';
}

showBtn.addEventListener('click', show);
function show() {
  workersDiv.innerHTML = '';
  workers.forEach(worker => {
    const workerDiv = document.createElement('div');
    workerDiv.className = 'worker';
    for (let prop in worker) {
      let propPara = document.createElement('p');
      propPara.innerHTML = `<strong>${prop}: </strong>${worker[prop]}`
      workerDiv.append(propPara);
      }
      workersDiv.append(workerDiv);
  })
}

// 4. Для предыдущего задания создайте функцию, которая будет принимать в себя массив объектов-сотрудников, и каждому из объектов
//    будет добавлять новое свойство "salary", хранящее зарплату сотрудника. 
//    Зарплата расчитывается, исходя из значения свойства "occupation" следующим образом:
//        • "director" — 3000;
//        • "manager" — 1500;
//        • "programmer" — 2000;
//        • для остальных значений — 1000.
//    После выполнения функции — вывести информацию о сотрудниках.

const salaryBtn = document.getElementById('salary-btn');
salaryBtn.addEventListener('click', addSalary);

function addSalary() {
  if (workers.length > 0) {
    workers.forEach(worker => {
      switch (worker.occupation) {
        case 'director': worker.salary = 3000
        break;
        case 'manager': worker.salary = 1500
        break;
        case 'programmer': worker.salary = 2000
        break;
        default: worker.salary = 1000
      }
    })
    show();
  }
}
        
// 5. Для задания 3 создать метод, позволяющий отсортировать массив сотрудников по одному из свойств: name, sName, age, occupation, salary.
//    Параметр для сортировки принимается от пользователя.
//    После выполнения функции — вывести информацию о сотрудниках.

const selectSort = document.querySelector('.select-sort');
selectSort.addEventListener('change', (e) => {
  if (workers.length > 0) {
    switch (e.target.value) {
      case 'name': 
        sortByName();
        break;
      case 'name': 
        sortByName();
        break;
      case 'surname': 
        sortBySurname();
        break;
      case 'age': 
        sortByAge();
        break;
      case 'occupation': 
        sortByOccupation();
        break;
      case 'salary': 
        sortBySalary();
        break;
    }
  }
})

function sortByName() {
  workers.sort((a, b) => {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    // names must be equal
    return 0;
  });
  show();
}

function sortBySurname() {
  workers.sort((a, b) => {
    const surnameA = a.surname.toUpperCase(); // ignore upper and lowercase
    const surnameB = b.surname.toUpperCase(); // ignore upper and lowercase
    if (surnameA < surnameB) {
      return -1;
    }
    if (surnameA > surnameB) {
      return 1;
    }
    // names must be equal
    return 0;
  });
  show();
}

function sortByAge() {
  workers.sort((a, b) => a.age - b.age);
  show();
}

function sortByOccupation() {
  workers.sort((a, b) => {
    const occupationA = a.occupation; // ignore upper and lowercase
    const occupationB = b.occupation; // ignore upper and lowercase
    if (occupationA < occupationB) {
      return -1;
    }
    if (occupationA > occupationB) {
      return 1;
    }
    // names must be equal
    return 0;
  });
  show();
}

function sortBySalary() {
  workers.sort((a, b) => a.salary - b.salary);
  show();
}