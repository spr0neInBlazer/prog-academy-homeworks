const currency1 = document.getElementById('user-input1');
const currency2 = document.getElementById('user-input2');
const header = document.querySelector('.header');
const grid = document.querySelector('.grid');
const amountInput = document.getElementById('currency-amount');
const output = document.querySelector(".output");
const toggleBtn = document.querySelector(".toggle-btn");
let chosenCurrency1;
let chosenCurrency2;
let data; 
let CC = [];

fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
  .then(response => response.json())
  .then(json => {
    data = json;
    data.map(elem => {
      CC.push(elem.cc);
    });
    handleData();
    autocomplete(currency1, CC);
    autocomplete(currency2, CC);
});

function handleData() {
  header.innerHTML = `<em>Last Update: ${data[0].exchangedate}</em>`;

  const gridContent = data.map(elem => 
    `<div class="grid_item">
      <div class="grid_cc"><strong>${elem.cc}</strong></div>
      <div class="grid_txt">${elem.txt}</div>
      <div class="grid_rate">${elem.rate}</div>
    </div>`
  ).join('');
  grid.innerHTML = gridContent;

  // set currency by default
  currency1.value = data[0].cc;
  currency2.value = data[1].cc;
  // set rates by default
  chosenCurrency1 = data[0].rate;
  chosenCurrency2 = data[1].rate;

  [currency1, currency2].forEach(input => {
    input.addEventListener('input', () => {
      input.value = input.value.toUpperCase();
      if (input === currency1) {
        chosenCurrency1 = data.find(el => el.cc === currency1.value)?.rate;
      } else {
        chosenCurrency2 = data.find(el => el.cc === currency2.value)?.rate;
      }
      getResult();
    })
  })
}

amountInput.addEventListener('input', getResult);

toggleBtn.addEventListener("click", () => {
  [currency1.value, currency2.value] = [currency2.value, currency1.value];
  [chosenCurrency1, chosenCurrency2] = [chosenCurrency2, chosenCurrency1];
  getResult();
});

function getResult() {
  if (!currency1.value || !currency2.value || !chosenCurrency1 || !chosenCurrency2) {
    output.innerHTML = 'Please enter correct currency';
  } else {
    const result = chosenCurrency1 / chosenCurrency2 * amountInput.value;
    output.innerHTML = isNaN(result) ? 'Please enter correct values' : result.toFixed(2);
  }
}

// autocomplete function implemented from https://www.w3schools.com/howto/howto_js_autocomplete.asp
function autocomplete(inp, arr) {
  let currentFocus;
  inp.addEventListener("input", handleInput);
  inp.addEventListener("keydown", handleKeyDown);
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
  
  function handleInput() {
    const val = this.value;
    closeAllLists();
    if (!val) return false;
    currentFocus = -1;
    const autocompleteList = document.createElement("div");
    autocompleteList.setAttribute("id", this.id + "autocomplete-list");
    autocompleteList.setAttribute("class", "autocomplete-items");
    this.parentNode.appendChild(autocompleteList);

    for (let i = 0; i < arr.length; i++) {
      // if input matches an item in dropdown menu 
      if (arr[i].substr(0, val.length).toUpperCase() === val.toUpperCase()) {
        const listItem = createListItem(arr[i], val);
        listItem.addEventListener("click", handleListItemClick);
        autocompleteList.appendChild(listItem);
      }
    }
  }

  function handleKeyDown(e) {
    let list = document.getElementById(this.id + "autocomplete-list");
      if (list) list = list.getElementsByTagName("div");
      if (e.keyCode == 40) { // Down
        currentFocus++;
        addActive(list);
      } else if (e.keyCode == 38) { // Up
        currentFocus--;
        addActive(list);
      } else if (e.keyCode == 13) { // Enter
        e.preventDefault();
        if (currentFocus > -1) {
          if (list) list[currentFocus].click();
        }
      }
  }

  function handleListItemClick(e) {
    inp.value = this.getElementsByTagName("input")[0].value;
    // set new rate if currency was selected by click or Enter press from dropdown menu
    if (inp === currency1) {
      chosenCurrency1 = data.find(el => el.cc === currency1.value)?.rate;
    } else {
      chosenCurrency2 = data.find(el => el.cc === currency2.value)?.rate;
    }
    closeAllLists();
    getResult();
  }

  function createListItem(text, val) {
    const listItem = document.createElement("div");
    listItem.innerHTML = `<strong>${text.substr(0, val.length)}</strong>`;
    listItem.innerHTML += text.substr(val.length);
    listItem.innerHTML += `<input type='hidden' value='${text}'>`;
    return listItem;
  }

  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    for (let i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(el) {
    const list = document.getElementsByClassName("autocomplete-items");
    for (let i = 0; i < list.length; i++) {
      if (el != list[i] && el != inp) {
        list[i].parentNode.removeChild(list[i]);
      }
    }
  }
}
