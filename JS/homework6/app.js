const number = document.querySelectorAll('.number');
const inp = document.querySelector('.calculator__inp');
const sign  = document.querySelectorAll('.sign');
const result = document.querySelector('.result');
const clearBtn = document.getElementById('clear');
const toggleBtn = document.getElementById('toggleSign');
const percentBtn = document.getElementById('percent');
const closeBtn = document.querySelector('.close-btn');

// close instructions message
// closeBtn.addEventListener('click', () => {
//   document.querySelector('.instructions').style.display = 'none';
// })

// clear input
clearBtn.addEventListener('click', () => {
  inp.value = '';
})

for(let i = 0; i < number.length; i++) {
  number[i].addEventListener('click', insertValue);
}

for(let i = 0; i < sign.length; i++) {
  sign[i].addEventListener('click', insertValue);
  sign[i].addEventListener('click', checkForNoNums);
}

function insertValue() {
  inp.value += this.innerText;
}

result.addEventListener('click', getResult);

function getResult() {
  if (/.+\/0$/.test(inp.value)) {
   alert("Can't divide by zero"); 
   inp.value = '';
  } else {
    inp.value = eval(inp.value);
  }
}

// '+/-' button
toggleBtn.addEventListener('click', changeSign);

function changeSign() {
  let arr = inp.value.split('')
  if (arr[0] == '-') {
    arr.shift();
  } else {
    arr.unshift('-');
  }
  inp.value = arr.join('');
}

function checkForNoNums() {
  if (/^[\D\D]/.test(inp.value)) {
    let arr = inp.value.split('');
    arr.pop();
    inp.value = arr.join('');
  }
}

// '%' button
percentBtn.addEventListener('click', getPercentage)


function getPercentage() {
  let referenceInput = inp.value;
  let tempArr = inp.value.split('');
  // if number is negative, remove '-' from input
  if (referenceInput.split('')[0] == '-') {
    tempArr.shift();
    inp.value = tempArr.join('')
  }
  // get operation sign
  let currentSign = inp.value.match(/[\+|\-|\*|\/]/).join('');
  let firstNumber = tempArr.slice(0, tempArr.indexOf(currentSign));
  // if number was negative, retrieve '-' sign
  if (referenceInput.split('')[0] == '-') {
    firstNumber.unshift('-');
  }
  // split array to get the number after the sign
  let arr = inp.value.split(/[^0-9\.]/);
  // get the percentage part of the number
  let secondNumber;
  if (currentSign == '+' || currentSign == '-') {
    secondNumber = [...firstNumber];
    secondNumber.push('\*', arr[arr.length - 1], '\/100');
    secondNumber = [eval(secondNumber.join(''))];
  } else {
    secondNumber = arr[arr.length - 1] / 100;
    secondNumber = secondNumber.toString();
  }
  firstNumber = firstNumber.join('');
  inp.value = firstNumber.concat(currentSign, secondNumber);
}

// perfrom only one operation at a time
sign.forEach((btn) => {
  btn.addEventListener('click', checkOneOperation);
  btn.addEventListener('click', checkForTwoSigns);
  // btn.addEventListener('click', checkForNoNums);
})

function checkOneOperation() {
  let signs = /[\+\-\*\/].+[\+\-\*\/]$/;
  if (signs.test(inp.value)) {
    let arr = inp.value.split('');
    let sign = arr[arr.length - 1];
    arr.pop();
    inp.value = arr.join('');
    
    getResult();
    arr = [inp.value, sign];
    inp.value = arr.join('');
  }
}

function checkForTwoSigns() {
  let arr = inp.value.split('');
  let forbiddenCombos = [
    ['*', '/'], ['/', '*'], ['-', '+'], ['+', '+'], ['/', '/'], ['*', '-'], ['*', '+'], ['/', '-'], ['+', '*'], ['+', '/'], ['-', '*'], ['-', '/'], ['/', '+']
  ];
  for (let i = 0; i < forbiddenCombos.length; i++) {
    if (arr[arr.length - 2] === forbiddenCombos[i][0] && arr[arr.length - 1] === forbiddenCombos[i][1]) {
      arr.pop();
      inp.value = arr.join('');
    }
  }
}

let symbols = /[^0-9\+\-\*\/\.%=]/
document.body.addEventListener('keydown',function(e) {
  // попробовать switch case

  if (e.key == 'Enter' || e.key == '=') {
    checkForNoNums();
    getResult();
  } else if (e.key == '%') {
    // don't enter '%' symbol into input
    e.preventDefault();
    getPercentage();
  } else if(e.key === 'a') {
    e.preventDefault();
    inp.value = '';
  }
  // prevent entering forbidden symbols
  else if (symbols.test(e.key) && e.key != 'ArrowLeft' && e.key != 'ArrowRight' && e.key != 'ArrowUp' && e.key != 'ArrowDown' && e.key != 'Backspace' && e.key != 'F5' && e.key != 'Delete') {
    e.preventDefault();
  }

  else if (e.key === '+' || e.key === '/') {
    checkForNoNums();
  }
})
