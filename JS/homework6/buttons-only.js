// запретить ввод 2 знаков, кроме ** (отложить пока)

const number = document.querySelectorAll('.number');
const dot = document.getElementById('dot');
const inp = document.querySelector('.calculator__inp');
const sign  = document.querySelectorAll('.sign');
const result = document.querySelector('.result');
const clearBtn = document.getElementById('clear');
const toggleBtn = document.getElementById('toggleSign');
const percentBtn = document.getElementById('percent');
// const closeBtn = document.querySelector('.close-btn');

// close instructions message
// closeBtn.addEventListener('click', () => {
//   document.querySelector('.instructions').style.display = 'none';
// })

// clear input
clearBtn.addEventListener('click', () => {
  inp.value = '0';
})

for(let i = 0; i < number.length; i++) {
  number[i].addEventListener('click', insertValue);
  number[i].addEventListener('click', checkFirstInput);
}

sign.forEach((btn) => {
  btn.addEventListener('click', insertValue);
  btn.addEventListener('click', changeSign);
  btn.addEventListener('click', checkOneOperation);
})

function insertValue() {
  inp.value += this.innerText;
}

function checkFirstInput() {
  // prevent entering 00
  if (/^[0]+$/.test(inp.value)) {
    inp.value = '0';
  } 
  // else replace '0' with the entered value
  else if (/^[0][1-9]/.test(inp.value)) {
    inp.value = this.innerText;
  }
}

// when 2 signs entered consecutively, display the latter one
function changeSign() {
  if (/[\+|\-|\*|\/]{2,}$/.test(inp.value)) {
    inp.value = inp.value.replace(inp.value[inp.value.length - 2], inp.value[inp.value.length - 1]);
    let arr = inp.value.split('');
    arr.pop();
    inp.value = arr.join(''); 
  }
}

result.addEventListener('click', getResult);

function getResult() {
  // handle dividing by zero
  if (/.+\/0$/.test(inp.value)) {
    alert("Can't divide by zero"); 
    inp.value = '0';
  } else {
    // floating point error fix
    let factor = Math.pow(10, 15);
    inp.value = Math.round(eval(inp.value) * factor) / factor;
  }
  // 
  // if (/\.0{5,}$/.test(inp.value)) {
  //   Number.parseFloat(inp.value).toFixed(2);
  // } else {
  //   // ИСПРАВИТЬ-----------------------------------
  //   console.log('<0')
  // }
}

// '+/-' button
toggleBtn.addEventListener('click', toggleSign);

function toggleSign() {
  let arr = inp.value.split('')
  if (arr[0] == '-') {
    arr.shift();
  } else {
    arr.unshift('-');
  }
  inp.value = arr.join('');
}

// function checkForNoNums() {
//   if (/^[\D\D]/.test(inp.value)) {
//     let arr = inp.value.split('');
//     arr.pop();
//     inp.value = arr.join('');
//   }
// }

// '.' button
dot.addEventListener('click', insertValue);
dot.addEventListener('click', () => {
  // prevent entering multiple consecutive dots (.......)
  if (/\.+/.test(inp.value)) {
    inp.value = inp.value.replace(/\.{2}/, '.');

    // prevent entering more than 1 dot in 1 number (1.2.3.4.)
    if (/\.\d+\./.test(inp.value)) {
      inp.value = inp.value.replace(/\.$/, '');
    }
  }
  
})

// '%' button
percentBtn.addEventListener('click', getPercentage)


function getPercentage() {
  let referenceInput = inp.value;
  let tempArr = inp.value.split('');
  // if number is negative, remove '-' from input
  if (referenceInput.split('')[0] == '-') {
    tempArr.shift();
    inp.value = tempArr.join('')
  }
  // get operation sign
  let currentSign = inp.value.match(/[\+|\-|\*|\/]/).join('');
  let firstNumber = tempArr.slice(0, tempArr.indexOf(currentSign));
  // if number was negative, retrieve '-' sign
  if (referenceInput.split('')[0] == '-') {
    firstNumber.unshift('-');
  }
  // split array to get the number after the sign
  let arr = inp.value.split(/[^0-9\.]/);
  // get the percentage part of the number
  let secondNumber;
  if (currentSign == '+' || currentSign == '-') {
    secondNumber = [...firstNumber];
    secondNumber.push('\*', arr[arr.length - 1], '\/100');
    secondNumber = [eval(secondNumber.join(''))];
  } else {
    secondNumber = arr[arr.length - 1] / 100;
    secondNumber = secondNumber.toString();
  }
  firstNumber = firstNumber.join('');
  inp.value = firstNumber.concat(currentSign, secondNumber);
}

function checkOneOperation() {
  let signs = /[\+\-\*\/].+[\+\-\*\/]$/;
  if (signs.test(inp.value)) {
    let arr = inp.value.split('');
    let sign = arr[arr.length - 1];
    arr.pop();
    inp.value = arr.join('');
    
    getResult();
    arr = [inp.value, sign];
    inp.value = arr.join('');
  }
  console.log('inside')
}

// function checkForTwoSigns() {
//   let arr = inp.value.split('');
//   let forbiddenCombos = [
//     ['*', '/'], ['/', '*'], ['-', '+'], ['+', '+'], ['/', '/'], ['*', '-'], ['*', '+'], ['/', '-'], ['+', '*'], ['+', '/'], ['-', '*'], ['-', '/'], ['/', '+']
//   ];
//   for (let i = 0; i < forbiddenCombos.length; i++) {
//     if (arr[arr.length - 2] === forbiddenCombos[i][0] && arr[arr.length - 1] === forbiddenCombos[i][1]) {
//       arr.pop();
//       inp.value = arr.join('');
//     }
//   }
// }

document.body.addEventListener('keydown',function(e) {
  // попробовать switch case
  checkFirstInput();
  if (e.key == 'Enter' || e.key == '=') {
    getResult();
  } else if (e.key == '%') {
    // don't enter '%' symbol into input
    e.preventDefault();
    getPercentage();
  } else if(e.key === 'a') {
    e.preventDefault();
    inp.value = '';
  }
  // prevent entering forbidden symbols
  // else if (symbols.test(e.key) && e.key != 'ArrowLeft' && e.key != 'ArrowRight' && e.key != 'ArrowUp' && e.key != 'ArrowDown' && e.key != 'Backspace' && e.key != 'F5' && e.key != 'Delete') {
  //   e.preventDefault();
  // }

  // else if (e.key === '+' || e.key === '/') {
  //   checkForNoNums();
  // }
})