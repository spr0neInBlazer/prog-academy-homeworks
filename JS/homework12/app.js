const container = document.querySelector('.container');
const mainSection = document.querySelector('.main-section');
const keyInfoDiv = document.getElementById('key-info');

const keyTitleCode = document.getElementById('key-title-code');
const keyCodeInfo = keyInfoDiv.querySelector('.key-code');
const keyInfoTitle = keyInfoDiv.querySelector('.info-title');

window.addEventListener('keyup', (e) => {
  e.preventDefault();
  updatePageContent(); // 2) я нажал клавишу, вызываю функцию, чтоб убрать сообщение и отобразить хар-ки клавиши
  displayKeyInfo(e);
  displayKeyEvent(e);
  displayKeyCode(e);
  displayKeyWhich(e);
  displayHistory(e);
});

let invokedFunction = false; // 1) пока значение false, показывать только вступительное сообщение
function updatePageContent() {
  if (!invokedFunction) { // 3) если 
    invokedFunction = true;
    let eventsGrid = `
    <div class="events-grid">
      <div id="key-event" class="event-container">
        <h5 class="event-title">event.key</h5>
        <div class="event-value">
        </div>
      </div>
      <div id="key-code" class="event-container">
        <h5 class="event-title">event.code</h5>
        <div class="event-value">
        </div>
      </div>
      <div id="key-which" class="event-container">
        <h5 class="event-title">event.which</h5>
        <div class="event-value">
        </div>
      </div>
      <div id="key-history" class="event-container">
        <h5 class="event-title">History</h5>
        <div class="history-events-grid">
        </div>
      </div>
    </div>`
    mainSection.innerHTML = eventsGrid;
  }
}

function displayKeyInfo(e) {
  keyTitleCode.innerText = e.which;
  keyCodeInfo.innerText = e.which;
  keyInfoTitle.innerText = 'Key Code Information';
}

function displayKeyEvent(e) {
  const keyEvent = document.getElementById('key-event');
  keyEvent.querySelector('.event-value').innerText = e.key;
}

function displayKeyCode(e) {
  const keyEvent = document.getElementById('key-code');
  keyEvent.querySelector('.event-value').innerText = e.code;
}

function displayKeyWhich(e) {
  const keyEvent = document.getElementById('key-which');
  keyEvent.querySelector('.event-value').innerText = e.which;
}

let btnArray = [];
function displayHistory(e) {
  let currentKeyObj = {
    key: e.key,
    code: e.code,
    which: e.which,
  }
  const keyHistory = document.getElementById('key-history');
  const btnsDiv = keyHistory.querySelector('.history-events-grid');

  // ignore if the pressed key is already at the beginning
  if (!btnArray[0] || btnArray[0].code !== e.code) {
    // if array is bigger than 4 and there's no repeated keys
    if (btnArray.length > 3 && !btnArray.find(btn => btn.code === e.code)) {
      btnArray.pop();
    }
    // if a key is already in the array  
    if (btnArray.find(btn => btn.code === e.code)) {
      btnArray = btnArray.filter(el => el.code !== e.code);
    }
    // put current key at the beginning
    btnArray.unshift(currentKeyObj);
    btnsDiv.innerHTML = '';
    btnArray.map(btn => {
      const historyKeyDiv = document.createElement('button');
      historyKeyDiv.className = 'history-key';
      historyKeyDiv.innerText = btn.code;
      btnsDiv.appendChild(historyKeyDiv);
    });
  }
}

// update key info on click in history section
mainSection.addEventListener('click', (e) => {
  if (e.target.classList.contains('history-key')) {
    let clickedBtn = e.target;
    let currentKey = btnArray.find(el => el.code === clickedBtn.innerText);
    displayKeyInfo(currentKey);
    displayKeyEvent(currentKey);
    displayKeyCode(currentKey);
    displayKeyWhich(currentKey);
  }
});