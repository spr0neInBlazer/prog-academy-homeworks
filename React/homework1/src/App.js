import React, {useState, useEffect} from 'react';
import Loading from './components/Loading';

const url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';

function App() {
  // loading screen until data is fetched
  const [loading, setLoading] = useState(true);
  const [currency, setCurrency] = useState([]);
  const fetchCurrency = async() => {
    try {
      const response = await fetch(url);
      const currency = await response.json();
      setLoading(false);
      setCurrency(currency)
    } catch (error) {
      setLoading(false);
      console.log(error)
    }
  }

  useEffect(() => {
    fetchCurrency();
  }, []);

  if (loading) {
    return (
      <main>
        <Loading />
      </main>
    )
  }

  return (
    <main>
      <h3>Last update: {currency[0].exchangedate}</h3>
      <div className="table-responsive">
        <table className='table table-striped table-bordered w-50'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Rate</th>
              <th>Abbreviation</th>
            </tr>
          </thead>
          <tbody className='table-group-divider'>
            {currency.map(cur => {
              return (
                <tr key={cur.r030}>
                  <td>{cur.txt}</td>
                  <td>{cur.rate}</td>
                  <td>{cur.cc}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </main>
  );
}

export default App;