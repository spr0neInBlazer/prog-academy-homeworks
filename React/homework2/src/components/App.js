import React, { useState, useEffect } from "react";
import Loading from "./Loading";
import Card from "./Card";
import "../app.css"

const urls = [
  'https://swapi.dev/api/people/1/',
  'https://swapi.dev/api/people/2/',
  'https://swapi.dev/api/people/3/',
  'https://swapi.dev/api/people/4/',
  'https://swapi.dev/api/people/5/',
  'https://swapi.dev/api/people/6/',
  'https://swapi.dev/api/people/7/',
  'https://swapi.dev/api/people/8/',
  'https://swapi.dev/api/people/9/',
  'https://swapi.dev/api/people/10/'
];

function App() {
  // loading screen
  const [loading, setLoading] = useState(true);
  const [people, setPeople] = useState();

  const fetchData = async(url) => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      return data;
    } catch (error) {
      setLoading(false);
      console.log(error)
    }
  }

  useEffect(() => {
    Promise.all(urls.map(fetchData))
      .then(data => {
        setPeople(data);
        setLoading(false);
      })
  }, []);

  if (loading) {
    return <Loading />
  }

  return (
    <div className="App">
      <h1>Star Wars App</h1>
      <div className="cards-container">
        {people.map(person => {
          return <Card person={person} key={person.name} />
        }
        )}
      </div>
    </div>
  );
}

export default App;