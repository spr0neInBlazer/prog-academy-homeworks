import React from 'react'

export default function Loading() {
  return (
    <div style={{color: 'white', textAlign: 'center'}}>
      <h2>Loading...</h2>
    </div>
  )
}