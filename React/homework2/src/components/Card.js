import React from 'react'

export default function Card({ person }) {
  const {
    name,
    height,
    mass,
    hair_color,
    skin_color,
    eye_color,
    birth_year,
    gender,
    url,
  } = person;

  return (
    <div className="character-card">
      <div className="character-container">
        <h3>{name}</h3>
          <div className="stats">
            <p><strong>Height: </strong>{height}</p>
            <p><strong>Mass: </strong>{mass}</p>
            <p><strong>Hair color: </strong>{hair_color}</p>
            <p><strong>Skin color: </strong>{skin_color}</p>
            <p><strong>Eye color: </strong>{eye_color}</p>
            <p><strong>Birth year: </strong>{birth_year}</p>
            <p><strong>Gender: </strong>{gender}</p>
          </div>
          <a href={url} target="_blank">Link</a>
      </div>
    </div>
  )
}